const fs = require("fs");
const fse = require("fs-extra");
const { exec } = require("child_process");
const chalk = require("chalk");
const marked = require("marked");

const log = console.log;

function numberTheCodeBlocks(html) {
	const pre_tags = html.match(/<pre>([\n\r]|.)*?<\/pre>/g);

	if(pre_tags)
		pre_tags.forEach(elt => {
			console.error(`${elt}`)
			const code_tag = elt.match(/<code>([\n\r]|.)*<\/code>/);

			html = html.replace(
				elt,
				"<pre>" + (code_tag ? code_tag[0]
				.split("\n")
				.slice(0,-1)
				.map(line => "<code>"+line.trim().replace("<code>", "")+"</code>")
				.join("\n") : "")
				+"</pre>"
			)
		});

	return html;
}


function renderMarkdownToHTML(input, output, navBar=true) {
	log(chalk.green(`Rendering ${input}`))

	// Get the base path
	const BASE_PATH = "";
	log(`- setting base path to - ${BASE_PATH}`)

	// Read the input file into the memory
	let md = fs.readFileSync(input, "utf8");

	if(md) {
		// Get the date at which the input file was last modified
		const lastModifiedAt = fs.statSync(input).mtime;
		const createdAt = fs.statSync(input).birthtime;

		// Get the word count of the file
		const sector_index = 1;//md.match(/\$word-count-\d*/).split("-")[2] | 1;
		const wordCount = md.split("---")[sector_index % md.split("---").length].split(" ").length;

		// Get the title
		let title;
		if (md[0] === "#") {
			title = md.match(/#* +.+(\n|\r)/)[0].slice(2, -1) || "title";
		} else {
			let lines = md.split("\n");
			title = lines[0] || "title";
			md = lines.slice(1).join("\n");
		}
		log(`- setting title to - ${title}`);

		// Get the logo
		const logo_url_line = md.match(/!logo\-url\(.*\)/);
		const logo_url = logo_url_line ? logo_url_line[0].split("(")[1].split(")")[0] : "";
		md = md.replace(logo_url_line ? logo_url_line[0] : "", " ");
		log(`- logo url: ${logo_url}`);

		// Get the description
		const description = md.indexOf("---") !== -1 ? md.split("---")[0].split("\n").slice(1, -1).join("\n").trim() : null || "Blog by Dimitri Tabatadze";
		log(`- description: ${description}`);

		// Render the markdown to HTML
		const markdownContent = numberTheCodeBlocks(marked(md
			.replace("$last-modified", lastModifiedAt)
			.replace("$created", createdAt)
			.replace("$word-count", wordCount)
		));
		

		// Get the CSS path
		const CSSPath = `https://dimitri.ge/css/style.css`;
		log(`- setting css path to - ${CSSPath}`)

		// Set back button up
		const navBarHTML = navBar ?
	`<nav>
		<button onclick="window.history.back()">უკან</button>
		<button onclick="window.location.href = '${BASE_PATH}/'">მთავარი</button>
	</nav>` : "";

		let html = 
	`<!DOCTYPE html>
	<html lang=ka>
		<head>
			<link rel="stylesheet" type="text/css" href="${CSSPath}">
			<link rel="shortcut icon" href="favicon.ico">
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta name="description" content="${description}">
			<meta property="og:title" content="${title}">
			<meta property="og:type" content="website" />
			<meta property="og:image" content="${logo_url}" />
			<meta property="og:url" content="https://blog.dimitri.ge/" />
			<meta property="og:description" content="${description}">
			<title>${title}</title>
		</head>
		<body>
			<div id="content">
				${navBarHTML}
				${markdownContent}
			</div>
		</body>
	</html>`;

		log(chalk.blue(`- writing file to - ${output}...\n`));
		fs.writeFileSync(output, html);
	}
}

(()=>{
	if(!fs.existsSync("build"))
		fs.mkdirSync("build", (err) => (err) ? console.error(err) : log("build directory created"))

	renderMarkdownToHTML("README.md", "build/index.html", navBar=false);

	exec("cd posts ; find -name \"**.md\"", (err, stdout, stderr) => {
		const posts = stdout.split("\n");//fs.readdirSync("posts/", (err) => {if(err) log({err})});
		log("POSTS  - ", posts)

		if(posts){
			for(let post of posts){
				if(-1 !== post.indexOf(".")) {
					let path = post.split("/").slice(1).join("/").split(".")[0];
					let pathList = path.split("/");
					log("POST  - ", pathList)

					exec(`mkdir -p build/${path}`, () => {
						log(chalk.yellow(`Created directory build/${path}/`))
	
						let outFile = path.endsWith("index")
							? `build/${pathList.slice(0, pathList.length-1).join("/")}/index.html`
							: `build/${pathList.slice(0, pathList.length-0).join("/")}/index.html`;
						renderMarkdownToHTML(`posts/${path}.md`, outFile)
					})
				}
			}
		}
	})

	// Copy favicons
	const favicons = fs.readdirSync("posts/favicons/", (err) => {if(err) log({err})});

	if(favicons) for(let favicon of favicons) {
		const name = favicon.split(".")[0];

		if(fs.existsSync(`build/${name}`))
			fs.copyFileSync(`posts/favicons/${favicon}`, `build/${name}/favicon.ico`);
	}
	
	fse.copySync(
		'src/', 
		'build/', 
		{overwrite: true},
		(err) => console.error(err))

	log(chalk.greenBright("Site built successfully!\n"))
})();
