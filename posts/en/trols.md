# TRoLS - Tunable Rocket Landing Simulation

author: [Dimitri Tabatadze](https://dimitri.ge/#contact-me)

last updated: $last-modified

created at: $created

---

## Back story


I've wanted to make a robust, high-DOF, realistic rocket landing simulator for quite a while now (ever since I saw the Falcon Heavy test flight and the landing). I've had a few attempts. One of them is quite neat and worth of a show-off. It's a 1-DOF point-mass launch & langind simulator writen in python.

### [rocket-sim-1D](https://github.com/D-T-666/rocket-sim-1D)

[![plot from the simulator mentioned above](https://cloud.dimitri.ge/resources/rocket-sim-1D.png)](https://cloud.dimitri.ge/resources/rocket-sim-1D.png)

It's really cool in a sense that it's done almost entirely from scratch (only ploting is done with a library) and it also works.

The way it works is very simple:

(all the units used are in Metric)

1. It is given parameters such as:
	-	`dry mass of the rocket`
	- `mass of fuel`
	- `burn time of fule` (for how long can it burn until depletion)
	- `maximum` & `minimum thrust`
	- `target hover altitude` & `duration`
	- `maximum touchdown velocity`
	- `ascent thrust`
2. Then it runs through a state machine:
	1. take off
		- keep the thrust at the specified `ascent thrust`
		- if the predicted apogee in case of engine shutdown reaches just over the `target hover altitude`, switch to the state 2
	2. coast
		- if the altitude goes just below the target altitude, switch to the state 3
	3. hover
		- engage a (hand tuned) PID controler to controll the thrust
		- keep count of the seconds past state 2, if it's greater than `target hover duration`, switch to state 4
	4. free-fall
		- turn the engine off
		- if the predicted distance to zero velocity in case engine is fired at 95% thrust + a (small) constant is less than the current altitude switch to state 5
	5. guided landing
		- set the thrust to 95% just to leave a margin for later correction in case we overshot and need just a little more delta v
		- continuously check if we're either overshooting the ground altitude with the predicted zero-velocity altitude, and adjust thrust accordingly
		- if we reach the specified `maximum touchdown velocity`, switch to state 6
	6. hover landing
		- constantly adjust thrust so that the thrust to weight ratio is 1.0 at all times (basically hover)
		- if touchdown is detected, end the program

Dynamics is done in a pretty simple and not-so-accurate way. I just keep track of the altitude and velocity, and then conditionally add the thrust acceleration (along with gravity) to velocity. 

### Magic pictures and gifs

But ever since I saw [Thomas Godden](https://twitter.com/GoddenThomas)'s article [How SpaceX lands Starship (sort of)](https://thomas-godden.medium.com/how-spacex-lands-starship-sort-of-ee96cdde650b), I became even more fascinated with the idea that this wild and almost impossible thing is actually doable! And not just doable, but it's actually pretty simple.

There also was [Declan Murphy](https://twitter.com/decmurphy_)'s [FlightClub](https://flightclub.io/). An awesome flight trajectory calculator and a lot more.

