# Some people think that schoolchildren should be able to choose their teachers themselves.

word count: $word-count

---

We've all been to school and we've all probably been in quarrel with our teachers. This is the time when the idea of pupils choosing the teachers themselves first comes to mind. But this disagreement is not a good reason to switch teachers. In fact, it may even be a bad idea to get a new teacher at that point, since the pupil will get used to bad practices in life. You should never seize any kind of a relationship just because of a disagreement.

There also are cases of teachers being not so good at their jobs. They may be inadequate or incompetent. This is the time the teacher should be swapped with a different one. But how is the pupil supposed to konw the level of competence of their teacher, or if what the teacher says or does is adequate or not? For all we know, the pupil may be lying just to get out of a sticky situation they got themselves in. But we must also listen to what pupils have to say about their teachers, because they might very well be victims of child abuse.

Giving power to younglings is not a bad idea, but the power should still be moderated in some way. Other than that, I think it should be a given that people choose who they interact with on daily basis, even children.