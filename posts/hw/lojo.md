# Some people think that a job can be good even when the salary is low.

word count: $word-count

---

There primariliy are three types of good jobs:

1. The ones that pay well
2. The ones that you like doing
3. The ones that don't benefit you that much, but are neccessary

The jobs that people like doing, are all over the place. Some people like being Taxi drivers, and som like being surgeons. You will always find someone that would love to do the job you need done. Those poeple generaly don't go looking for high paying jobs, unless they need to support their families, because for them, all life is, is the job they want to take.

There also are people who are heroic and/or patriotic, who go to army for example. Those people may not want to do so, and they may not even get paied that well for that, but they still take the job, since they know that this job needs to be done, and *who's gonna do it but them?*.

However, there also are people who do jobs just for the money. People usually do this so that they can support their families, or to just live a wealthy life.

In the end what matters is that no matter the job, you will probably find someone who would do it even with below minimum wage (not that you should though).