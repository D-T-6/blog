# Some people think that a mobile phone is the best invention made by a man

word count: $word-count

---

There have been quite a few inventions that have made people of it's time think that it was the best invention ever. There's a lot of different ways of viewing something like an invention as *the best ever*, but not all of them are generally accepted as objective. There is not a single actually objectively best invention, sicne every single one of them is built on top of the previous ones. Take a wheel for example. We all know that not having invented the wheel would have been very detrimental to humanity's development. But to say that it's the best we've done, would be a disgrace to the hundreds or thousands of years people spent mastering the carving of stone and wood. Without those, we wouldn't have the all-beloved wheel.

Same goes for mobile phones. Phones are built on millenias of technological evolution. To say that mobile phones are the best, would be a disgrace to all the components that go into making a single mobile device, such as the LCD displays, the silicon chips consisting of billions of transistors, the mass-manufacturing lines, which would be impossible without very advanced robotics and human coordination skills, and so on.

But there's only one thing that is not built on top of previous inventions or discoveries: language. Without language, there would be no inventions. Language, is the onyl universally and probably the most objectively good invention of human kind. This is true for all languages, since all of them are basically based on each other.