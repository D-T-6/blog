# Some people think that children can get good education at home without going to school

word count: $word-count

---

Homeschooling is something that's still actively debated upon. Some people, especially parents, have strong opinions about the subject. Some of them believe that children should go to school not only for the education, but also for the social interaction with their peers. This would be done to ensure child's social development. Some believe that schools should only provide education, and they should not focus on anything else. But schools being full of children, that's quite impossible. So why not just provide the same, or even better education at home, without any of those pesky and irritating disractions?

I think that this case is complex ehough, that we should have individual parents make their own judgements. We shouldn't force childrent to go to school, or to not go to school. Because some children do require school-like environments where they can develop their social skills, but some do not, and would benefit froma a more efficient way of education. I, for example, defenetly needed to go to school, since I wasn't really that social of a child, but some of my friends, would absolutely be better off beign homeschooled.

In conclusion, there's not a lot that can be generalized about this subject. I neither agree or disagree with the opinion, since it's fully based on the child at hand. Based on the level of development of that child, I'd either agree or disagree with the provided statement.