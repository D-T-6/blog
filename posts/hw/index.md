# Homework

### English essays

- 15-12-21 : [Some people think that you can earn a good salary even when you do not have a university education](/hw/unsa)
- 13-12-21 : [Some people think that a mobile phone is the best invention made by a man](/hw/moma)
- 13-12-21 : [Some people think that schoolchildren should be able to choose their teachers themseves](/hw/pute)
- 10-12-21 : [Some people think that a job can be good even when the salary is low](/hw/lojo)
- 15-11-21 : [Some people think that computers will replace teachers in the near future](/hw/cote)
- 12-11-21 : [Some people think that money is the main thing that makes a person happy](/hw/moha)
- 09-11-21 : [Inner beauty is as important as outer beauty](/hw/inbe)
- 09-11-21 : [Some people think that children can get good education at home without going to school](/hw/hosc)
- 04-11-21 : [Some people think that newly married couples should live separately from their parents](/hw/macu)
- 02-11-21 : [Some people think that children should choose their parents' profession.](/hw/papr)
- 29-10-21 : [Some people think that pupils should not be given homework](/hw/puwo)
- 26-10-21 : [Some people think that teenagers should work and earn some money to help their parents](/hw/spttsw)

### Georgian essays

- 13-12-21 : [მიზნის მისაღწევად ძალადობის გამოყენება დაუშვებელია](/hw/vipu)
- 06-12-21 : [ჯერ ხარ მერე ქმნი - თვითგანმტკიცება წარმატების საწინდარია](/hw/xaqm)
- 05-11-21 : [კანონის დარღვევა სახელმწიფოებრივი ინტერესების გამო ზოგჯერ გამართლებადია](/hw/labr)
<!-- - 04-11-21 : [საკუთარი პრინციპების ერთგულება ადამიანს განსაკუთრებული სულიერი სიმტკიცით აღავსებს](/hw/stbe) -->
- 02-11-21 : [2022 წლის ეროვნული გამოცდების ნიმუში](/hw/nat21)
- 27-10-21 : [საზოგადოების ნაწილი მიიჩნევს, რომ სიტყვის თავისუფლება უფრო მნიშვნელოვანია ვიდრე პირადი ცხოვრების ხელშეუხებლობა](/hw/fosp)
