# Some people think that teenagers should work and earn some money to help their parents. Do you agree or disagree with this opinion?

Last edit: $last-modified

---

Questions like this can not really be given a general answer to. There are imeasurably many cultures and localities around the globe that will be biasing the opinions of the locals about the subject. For example, in most of the first world countries people will say that children should never be forced to do anything. But on the other hand, let's say in philipines, there's almost no way for families to feed their children if the teenagers themselves don't help out with earning the living.

But since you are asking me of *my* opinion, here it is: I don't think that children should either be forced to work to survive, but I also think that they should be permited to work if they want to. I am a teenager myself and I can say that the system I live under has a very bad way of handling such cases. For some ungodly reason I am not allowed to have a full time job at a company I can do the work for, with the only reason being my age. Sure, it's done to prevent child labor but it's also taking away freedom of choice from children - which in my humble opinion is very unaccaptable and unfavorable.

About helping thier own families, I strongly thing that the state should ensure that every family can *live* without people, and especially children being forced to work, something that may cause long term developmental issues. 