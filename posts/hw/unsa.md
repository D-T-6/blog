# Some people think that you can earn a good salary even when you do not have a university education

word count: $word-count

---

There's been a boom in the amount of self-tought profesionals reaching a lot of success since the internet and the so called *information age* came about. Poeple started gaining access to more and more different kinds and higher volumes of information, and so there came a day when everyone no matter their background, could master anything they wanted at the price of access to internet. This meant that people no longer needed to go to university to become experts of their subjects, they could just look materials up on the internet, and educate themselves. As this form of education started to gain traction, though, universities have had to either ramp up their quality of education by quite a big margin, or restrict access to the materials they provide their students with, so that people outside of their class can't access any of it. By doing this, they can guarantee their yearly income. However, big corporations restricting who gets to get good education and who doesn't, encourages rebelious people who fingt for justice to find more and more creative ways of obtaining study material of the same level of fedelity
as the ones you'd get for premium price. These individuals sometimes also create new material from scratch, and they put that material out on the internet for free!

Not only is it possible to acummulate knowledge and experience on your own without a college, big companies are now even no longer asking for university certificates for well paying job positions. This factually and objectively states that, yes, you can earn a good salary even when you don't have a university education. 