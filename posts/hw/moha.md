# Some people think that money is the main thing that makes a person happy

word count: $word-count

---

Every person that has ever lived, probably has a unique definition of success. For some people, success is just living a peacful, beautiful life with their small family. For some, it's making the most amount of people at least marginally happier. Being successful in their own way, is what makes a person happy. So if your definition of success requires a lot of money, money will probably be the main thing that *makes you happy*. But we all know that happiness doesn't last long. If you complete your life goals, but you still have a lot of time to live, you start making new goals, ones that may vary widely from the previous ones. This means that even if your goal is getting rich so that you can afford going to space, the requirement of money lasts as long as the desire for that goal. Even the *want* of money is dynamic and everchanging.

For me, personally, current *ultimate goal* is something that does require a moderate amount of wealth, so getting a lot of money, *would* make me happier. But That doesn't mean that money is my main source of happiness, it's just one of many.