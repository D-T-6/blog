# Some people think that pupils should not be given homework.
## Do you agree or disagree with this opinion?

last edit: $last-modified

created: $created

word count: $word-count

---

Probably all of us has heard the saying "practice makes perfect". That saying basically implies that more work you do, better the work you do becomes. And the saying being as old as time itself, means that it's probably true. We don't even need to trust some rusty sayings. We've all learned something, and we've probably observed that more you do something, you start doing the thing better. Like learning to ride a bike for example. First few attempts usually end misserably, but after about 3 tries, you manage to hold yourself upright.

I think doing homework is very much comparable to practicing how to ride a bike. You have to do the thing over and over again, with slight improvements with each iteration, and the final product will be much better than the initial product.

Although, this does mean that pupils will have much less free time when they get home from school. They will probably not be able to do the things they enjoy, and also will not be able to socialize as much. It's like the teacher's deciding what's good for the student, which is probably the right thing to do at very young ages, but as the pupils grow, we should be allowing them to make their own decisions. After all, independence is one of the most important qualities one can have.

To wrap this up, I think that if a pupil has fair and trustworthy judgement, they themselves should choose to do homework or not.