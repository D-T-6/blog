# Inner beauty is just as important as outer beauty

word count: $word-count

---

Latelys, since about the time the prude movement become prevelant, people started saying that inner beauty (or personality) is more important than the outer beauty. This although sounding like something very true, is probably just another propaganda. Person's personality is indubitably important. After all, personality is what we interact with when we talk to people, that's especially true in the modern world, where most of the communication takes place in text form over long distances.

But that doesn't discount the importance of visual beauty. It's really sad that we do, but we, humans, are nagurally attracted to visually beautiful people. This is becaude beauty, in most cases, signifies health and wealth, both of which are very important to us. Natural selection practically ensures that this feature is developed in every functioning society. And danying that you may be attracted to beautiful people is surface level and illogical.

In conclussion, I think that visual beauty is very important for starting a relationship and beautiful personality is veru important for maintaining it.
