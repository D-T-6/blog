# Some people think that newly married couples should live separately from their parents.
## Do you agree or disagree with this opinion?

word count: $word-count

---

People always make a fuss about living with parents as an adult, and this has always confused me. There's just so many good reasons to not move out of your paren't house even at age 40, that it seems uncomprehandably illogical to me that people get shocked when they hear about others living with their parents.

First of all, there's a lot of comfort with living with other people. Although, it's true that living with parent's is not as easy as livign with friends, or other kinds of relatives. Howevers, if you don't have friends to live with, parents' is the second best option. The amount of work distribution while being in a household of at least 2 is much better than that of a solo household. You get and get to help with minute chores and tasks that are deadly boring. And I think that is much worth the effort of having a little less alone time.

There also is the environmental factor. Having almost every individual live alone, is quite a costly thing for the mother nature. That's assuming that having about 7 billion individual, fully equipped houses is even possible. Everyone living alone is literally the most inefficient way of maintaining a proper society or even species. Having that happen would probably cause a very dramatic speed up of global warming, in term, possibly wiping out the whole population.

Only valid for living alone is the other kind of comfort it provides. Nobody will bother you, and you can do whatever you want, but lets be fair, who really wants total freedom? 