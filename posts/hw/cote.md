# Some people think that computers will replace teachers in the near future

word count: $word-count

---

If you asked this question to someone like me just a couple of years ago, I promise, you'd get a wildly different answer. That is because the COVID-19 pandemic has given us its best at forcing people to go digital on almost every single thing they do. This includes schools and all the other kinds of teaching facilities. Even though after the schools went on lockdown the teachers teaching students remained the same, I can almost fully confidently say that probably the majority of the students learnt more from online, non-school resources than from their own teachers. It's just simply illogical to not learn things that you learn at school for free, and on the same platform you go for entertainment. Plus the content creator you are learning something from is probably more fun and entertaining to listen to, and it probably has more visualizations that will help you better understand the subject. I myself enjoy helping people understand what I already understand well, and I have made a [tutorial or two](https://vid.dimitri.ge) for that reason.

I think that making life easier by making the lessons *re-usable* is a good idea, and I would love that to happen. Although it will aid in the decrease of jobs for classical teachers.