## Some people think that children should choose their parents' profession.

last edit: $last-modified

word count: $word-count

---

The choice of profession has never had as big of a pool as it does now. Almost anyone can pick whatever they want as their profession. But, although it may seem like it, this is not a good thing in every case. Children are basically forced to pick from a whole range of professions. This decission, in most cases, is made just to be regretted afterwards. So why let the immature *kids* make the decision for themselves? Why should we not the decision for them?

There are a lot of reasons to not do that. First, there is the factor of limiting the child's choice. What if that specific child is mature enough to decide for themselves? Who's to say that some children should get to decide and some should not? Then there also is a fair chance that the decision you make for the child is as bad as it would be in case the child had made it. Why should the child regret *your* decisiions instead of theirs? And by stating that children *should* choose their parents' professions, you're basically sayin that the decision should be made by the parents instead of the children themselves.

Even imagining someone making that important of a decision for me is too uncomfortable, so having that happen to me would be very bad. And who in their right mind would wish something that bad upon their fellow comrades?
