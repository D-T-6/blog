# დიმიტრის ბლოგი 🌸

[მთავარი გვერდი](https://dimitri.ge) | [English](/en)

ახლა დაჰოსტილი ჩემ საკუთარ სერვერზე! :D

## სტუმრები:

- [მარიამ ბლიაძე](/mb)

### პოსტები

- [Git-ის მოკლე მაგრამ მდგრადი თუთორიალი](/ge/git-started)
- [ტექნოლოგიური თავისუფლება და დამოუკიდებლობა](/ge/tech-freedom)
- [რას ნიშნავს „ღია წყარო“?](/ge/open-source)
